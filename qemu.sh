#! /bin/bash

BUSYBOX_PATH=${BUSYBOX_PATH:=../busybox-1.27.2}
ETC_PATH=${ETC_PATH:=etc}
LINUX_PATH=${LINUX_PATH:=../linux}
STRACE_PATH=${STRACE_PATH:=../strace-4.21}
TARGET_PATH=${TARGET_PATH:=../target}

export CROSS_COMPILE=arm-linux-gnueabi-
export ARCH=arm

rootfs_create()
{
	# create and mount FS
	dd if=/dev/zero of=$TARGET_PATH/root.fs bs=1M count=32
	mkfs.ext4 $TARGET_PATH/root.fs

	# prepare content
	mkdir $TARGET_PATH/root_dir
	sudo mount $TARGET_PATH/root.fs $TARGET_PATH/root_dir

	sudo cp -r $BUSYBOX_PATH/_install/* $TARGET_PATH/root_dir/
	sudo mkdir -p $TARGET_PATH/root_dir/{lib,dev,proc,sys,tmp,root,var}
	sudo cp -r /usr/arm-linux-gnueabi/lib/* $TARGET_PATH/root_dir/lib
	sudo cp -r $ETC_PATH $TARGET_PATH/root_dir/
	#for ko in `find ldd -name "*.ko"`; do sudo cp -r $ko $TARGET_PATH/root_dir/root; done
	for mod in `find ldd -name "modules"`; do sudo cp -r $mod $TARGET_PATH/root_dir/lib/; done
	sudo cp -r ldd/scull/scull.init $TARGET_PATH/root_dir/root
	sudo cp -r $STRACE_PATH/strace $TARGET_PATH/root_dir/usr/bin

	sudo mknod $TARGET_PATH/root_dir/dev/tty1 c 4 1
	sudo mknod $TARGET_PATH/root_dir/dev/tty2 c 4 2
	sudo mknod $TARGET_PATH/root_dir/dev/tty3 c 4 3
	sudo mknod $TARGET_PATH/root_dir/dev/tty4 c 4 4
	sudo mknod $TARGET_PATH/root_dir/dev/console c 5 1
	sudo mknod $TARGET_PATH/root_dir/dev/null c 1 3

	sudo umount $TARGET_PATH/root_dir
	rm -rf $TARGET_PATH/root_dir
}

target_update()
{
	#clear 
	[ -f $TARGET_PATH/image ] && rm $TARGET_PATH/image
	[ -f $TARGET_PATH/dtb ] && rm $TARGET_PATH/dtb
	[ -f $TARGET_PATH/root.fs ] && rm -rf $TARGET_PATH/root.fs

	#copy zImage && dtb
	cp $LINUX_PATH/arch/arm/boot/zImage $TARGET_PATH/image
	cp $LINUX_PATH/arch/arm/boot/dts/vexpress-v2p-ca9.dtb $TARGET_PATH/dtb

	#create rootfs
	rootfs_create
}

auto_build()
{
	if [ -z "$1" ]; then
		echo "Quit auto build!"
	fi

	if [ "$1" == "strace" ] || [ "$1" == "all" ]; then
		cd $STRACE_PATH
		./configure --build x86_64-pc-linux-gnu --host arm-linux-gnueabi LDFLAGS="-static"
		make
		file strace
		cd -
	fi

	if [ "$1" == "busybox" ] || [ "$1" == "all" ]; then
		cd $BUSYBOX_PATH
		make defconfig 

		#make CROSS_COMPILE=arm-linux-gnueabi- -j8
		#make install CROSS_COMPILE=arm-linux-gnueabi- 
		make -j8
		make install
		cd -
	fi

	if [ "$1" == "linux" ] || [ "$1" == "all" ]; then
		cd $LINUX_PATH
		#make CROSS_COMPILE=arm-linux-gnueabi- ARCH=arm vexpress_defconfig 
		#make CROSS_COMPILE=arm-linux-gnueabi- ARCH=arm menuconfig 
		# Disable: System Type -> [] Enable the L2x0 outer cache controller
		# Enable:  Kernel Features -> [*] Use the ARM EABI to compile the kernal
		#make CROSS_COMPILE=arm-linux-gnueabi- ARCH=arm -j8
		make vexpress_defconfig
		make menuconfig
		make -j8
		cd -
	fi

	if [ "$1" == "ldd" ]; then
		if [ "$2" == "all" ]; then
			cd ldd
			#make CROSS_COMPILE=arm-linux-gnueabi- ARCH=arm
			make
			cd -
		else
			linux_path=`realpath $LINUX_PATH`
			cd ldd/$2
			#KERNELDIR=$linux_path make CROSS_COMPILE=arm-linux-gnueabi- ARCH=arm
			KERNELDIR=$linux_path make
			cd -
		fi
	fi
}

qemu_run()
{

	qemu-system-arm -M vexpress-a9 -m 512M -nographic -sd $TARGET_PATH/root.fs -kernel $TARGET_PATH/image -append "root=/dev/mmcblk0 console=ttyAMA0 rw init=/linuxrc" -dtb $TARGET_PATH/dtb
	#-s -S	#for "-gdb tcp::1234" and pause
	#gdb -tui => target remote localhost:1234 => symbol-file vmlinux
}

if [ "$1" == "run" ]; then
	qemu_run
elif [ "$1" == "update" ]; then
	target_update
elif [ "$1" == "build" ]; then
	auto_build $2 $3
fi

