#! /bin/bash

######
# - ./qemu_rpi.sh
#	 - run normal raspi2b (arm)
# - ./qemu_rpi.sh -u
#	 - build kernel/rpi before running normal raspi2b (arm)
# - FIRSTRUN=1 ./qemu_rpi.sh
#    - run for the first time. It run firstrun.sh to bypass login and power off
# - [FIRSTRUN=1] ARCH=arm64 ./qemu_rpi.sh
#	 - run normal raspi3b (arm64)
######

export QEMU_RPI=1

TARGET_PATH=${TARGET_PATH:=../target/rpi}
LINUX_PATH=${LINUX_PATH:=../../linux-rpi/}

#ARCH=${ARCH:-arm64}
ARCH=${ARCH:-arm}

if [ "$ARCH" = "arm64" ]; then
	URL=https://downloads.raspberrypi.org/raspios_lite_arm64/images/raspios_lite_arm64-2023-05-03/2023-05-03-raspios-bullseye-arm64-lite.img.xz
	KERNEL_IMG=kernel8.img
	DTB_BIN=bcm2710-rpi-3-b.dtb
else
	URL=https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2023-05-03/2023-05-03-raspios-bullseye-armhf-lite.img.xz
	KERNEL_IMG=kernel7.img
	DTB_BIN=bcm2710-rpi-2-b.dtb
fi

XZ=$( basename $URL )
TARGET_IMG=${XZ%.xz}

# Download image
if ! [ -f ${TARGET_PATH}/$TARGET_IMG ]; then
	cd ${TARGET_PATH}
	wget $URL
	xz --decompress ${TARGET_IMG}.xz

	img_size=$( stat -c %s ${TARGET_IMG} )
	if [ $img_size -gt $(( 2 ** 31 )) ]; then 
		echo "[ERR]: img size $img_size"
		exit 1
	fi

	qemu-img resize -f raw $TARGET_IMG 2G
	cd -
fi

## Setup image - extract kernel.img and copy firstrun script
CMDLINE="rw earlycon=pl011,0x3f201000 console=ttyAMA0 loglevel=8 root=/dev/mmcblk0p2 fsck.repair=yes net.ifnames=0 rootwait memtest=1 dwc_otg.fiq_fsm_enable=0 debug"

if [ "$FIRSTRUN" = "1" ]; then
	MOUNT_DIR=/mnt/raspberrypi
	LOOP_DEV="$(sudo losetup -f)"
	LOOP_MAPPER=/dev/mapper/$(basename "$LOOP_DEV")
	FIRSTRUN_SCRIPT=firstrun.sh

	# Add mappings for all partitions in the raw disk img
	# "losetup -l", "mount", "lsblk", "df -h" to check
	sudo kpartx -av ${TARGET_PATH}/$TARGET_IMG

	# mount the partition mappings
	sudo mkdir -p ${MOUNT_DIR}
	sudo mount ${LOOP_MAPPER}p2 ${MOUNT_DIR}
	sudo mount ${LOOP_MAPPER}p1 ${MOUNT_DIR}/boot

	# Add user "coolen:kl345" for firstrun to pass the login
	cat >${FIRSTRUN_SCRIPT} <<EOF
#! /bin/bash

sudo useradd -m coolen
echo "coolen:kl345" | sudo chpasswd

exit 0
EOF
	sudo chmod 777 ${FIRSTRUN_SCRIPT}
	sudo cp ${FIRSTRUN_SCRIPT} ${MOUNT_DIR}/boot/
	rm ${FIRSTRUN_SCRIPT}

	# copy kernel img and dtb bin for backup
	sudo cp ${MOUNT_DIR}/boot/$KERNEL_IMG $TARGET_PATH
	sudo cp ${MOUNT_DIR}/boot/$DTB_BIN $TARGET_PATH

	# unmount and remove the image partition mappings
	sudo umount $MOUNT_DIR/boot
	sudo umount $MOUNT_DIR
	sudo rm $MOUNT_DIR -rf

	sudo dmsetup remove ${LOOP_MAPPER}p1
	sudo dmsetup remove ${LOOP_MAPPER}p2
	sudo losetup -d $LOOP_DEV

	# run firstrun.sh for the firstrun to bypass login
	# The action may be "none", "reboot", "exit".
	CMDLINE="${CMDLINE} systemd.run=/boot/${FIRSTRUN_SCRIPT} systemd.run_success_action=exit"
fi

## build and update kernel, dtb
if [ "$1" = "-u" ]; then
	if [ "$ARCH" = "arm64" ]; then
		echo "[ERR] Don't support to update kernel/dtb for arm64"
		exit 1
	fi

	if ! [ -d "$LINUX_PATH" ]; then
		echo "[ERR] $LINUX_PATH doesn't exist"
		exit 1
	fi

	cd $LINUX_PATH
		CROSS=1 ./autobuild.sh zImage
		CROSS=1 ./autobuild.sh dtbs
	cd -

	sudo cp $LINUX_PATH/OUT_C/arch/arm/boot/zImage $TARGET_PATH/$KERNEL_IMG
	sudo cp $LINUX_PATH/OUT_C/arch/arm/boot/dts/$DTB_BIN $TARGET_PATH/$DTB_BIN
fi

# Run Qemu with Image
if [ "$ARCH" = "arm64" ]; then
	qemu-system-aarch64 \
	    -M raspi3b \
	    -cpu cortex-a53 \
	    -m 1G \
	    -kernel "${TARGET_PATH}/${KERNEL_IMG}" \
	    -dtb "${TARGET_PATH}/${DTB_BIN}" \
	    -drive "file=${TARGET_PATH}/${TARGET_IMG},format=raw,index=0,media=disk" \
	    -append "${CMDLINE}" \
	    -usb \
	    -device usb-mouse \
	    -device usb-kbd \
	    -device usb-net,netdev=net0 \
	    -nographic \
	    -serial mon:stdio \
	    -netdev user,id=net0,hostfwd=tcp::7777-:22
else
	# disable usb-net for now
	qemu-system-arm \
	    -M raspi2b \
	    -m 1G \
	    -kernel "${TARGET_PATH}/${KERNEL_IMG}" \
	    -dtb "${TARGET_PATH}/${DTB_BIN}" \
	    -drive "file=${TARGET_PATH}/${TARGET_IMG},format=raw,index=0,media=disk" \
	    -append "${CMDLINE}" \
	    -usb \
	    -device usb-mouse \
	    -device usb-kbd \
	    -nographic \
	    -serial mon:stdio
		#-s -S	#for "-gdb tcp::1234" and pause
		#gdb -tui => target remote localhost:1234 => symbol-file vmlinux
		#gdb -tui vmlinux => target remote localhost:1234
fi
